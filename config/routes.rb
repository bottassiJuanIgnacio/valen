Rails.application.routes.draw do

  root 'homes#index'
  
  resources :themes
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

  get 'index', to: 'homes#index'
  get 'introcurso', to: 'homes#curso'
  get 'introfinal', to: 'homes#final'
  get 'about', to: 'homes#about'
  get 'download_pdf', to: 'homes#downloadCurso', as: 'download_pdf_curso'
  get 'download_pdf', to: 'homes#downloadFinal', as: 'download_pdf_final'



  get 'login', to: 'users_session#new'
  post 'login', to: 'users_session#create'
  post 'logout', to: 'users_session#destroy', as: :logout
  #delete 'logout' => 'users_session#destroy', as: :destroy_current_user_users_session

  get 'register', to: 'users_registration#new'
  post 'register', to: 'users_registration#create'

  get 'producto_cursadas', to: 'cursadas#indexProducts'
  get 'producto_cursadas/:id', to: 'cursadas#showProduct'

  get 'producto_finals', to: 'finals#indexProducts'
  get 'producto_finals/:id', to: 'finals#showProduct'

  resources :password_resets, only: [:create, :edit, :update,:new]
  scope :admin do
    resources :users, only: [:index,:update,:destroy]
    resources :cursadas do
      resources :cursada_contents
    end
    resources :finals, path: 'final' do
      resources :final_contents
    end
  end
end
