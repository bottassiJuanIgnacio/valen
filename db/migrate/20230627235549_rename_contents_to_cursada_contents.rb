class RenameContentsToCursadaContents < ActiveRecord::Migration[7.0]
  def change
    rename_table :contents, :cursada_contents
  end
end
