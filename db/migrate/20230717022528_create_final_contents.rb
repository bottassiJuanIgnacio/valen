class CreateFinalContents < ActiveRecord::Migration[7.0]
  def change
    create_table :final_contents do |t|
      t.string :title
      t.text :text
      t.string :description
      t.references :final, null: false, foreign_key: true

      t.timestamps
    end
  end
end
