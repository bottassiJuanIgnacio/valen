class CreateFinals < ActiveRecord::Migration[7.0]
  def change
    create_table :finals do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end
  end
end
