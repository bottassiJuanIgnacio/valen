class AddCursadaAndFinalToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :cursada, :boolean, default: false
    add_column :users, :final, :boolean, default: false
  end
end
