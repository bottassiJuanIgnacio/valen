# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
users  = [
    {
      first_name: "Administrator",
      last_name: "Administrator",
      email: "admin@gmail.com",
      password: "12345678",
      password_confirmation: "12345678",
      role: "administrador",
      cursada: true,
      final: true
    },
    {
      first_name: "Usuario",
      last_name: "Cursada",
      email: "cursada@gmail.com",
      password: "12345678",
      password_confirmation: "12345678",
      role: "estudiante",
      cursada: true,
      final: false
    },
    {
      first_name: "Usuario",
      last_name: "Final",
      email: "final@gmail.com",
      password: "12345678",
      password_confirmation: "12345678",
      role: "estudiante",
      cursada: false,
      final: true
    },
    {
      first_name: "Usuario",
      last_name: "Cursada Final",
      email: "cursadafinal@gmail.com",
      password: "12345678",
      password_confirmation: "12345678",
      role: "estudiante",
      cursada: true,
      final: true
    },
    {
      first_name: "Prueba",
      last_name: "Prueba",
      email: "fisiologiaprueba123@gmail.com",
      password: "12345678",
      password_confirmation: "12345678",
      role: "estudiante",
      cursada: false,
      final: false
    }
  ]
User.create users
#User.create(first_name: "Administrator", last_name: "Administrator", email: "admin@gmail.com", password: "12345678", password_confirmation: "12345678", role: "administrador", cursada: true, final: true)

#Seeds de Productos
Cursada.create(title:"Molidad", text: '<iframe src="https://drive.google.com/file/d/1bI_jDtE1CB_lWX2izvFDGJ0YPEe2ACjh/preview" width="640" height="480" allow="autoplay"></iframe>', description: "Esto es una descripción del producto")
Cursada.create(title:"CONTRACCIÓN MUSCULAR", text: '<iframe src="https://drive.google.com/file/d/1dP1vFc6XAblClimOPbj7knUKjXbCAVZ_/preview" width="640" height="480" allow="autoplay"></iframe>', description: "Esto es una descripcionl ")
Cursada.create(title:"Corazón", text: '<iframe src="https://drive.google.com/file/d/1emt8WHuMWnrwTDa92mF8B4bRW4wX6pq3/preview" width="640" height="480" allow="autoplay"></iframe>', description: "Esto es una descripción ")
Cursada.create(title:"CURSO DE FINAL", text: '<iframe width="560" height="315" src="https://www.youtube.com/embed/6kxcENOfVvw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>', description: "Esto es una descripción de yt ")

#Seeds de Productos
Final.create(title:"Modalidad", text: '<iframe src="https://drive.google.com/file/d/1bI_jDtE1CB_lWX2izvFDGJ0YPEe2ACjh/preview" width="640" height="480" allow="autoplay"></iframe>', description: "Esto es una descripción del producto")
Final.create(title:"CONTRACCIÓN MUSCULAR", text: '<iframe src="https://drive.google.com/file/d/1dP1vFc6XAblClimOPbj7knUKjXbCAVZ_/preview" width="640" height="480" allow="autoplay"></iframe>', description: "Esto es una descripcionl ")
Final.create(title:"Gráfico", text: '<iframe src="https://drive.google.com/file/d/1emt8WHuMWnrwTDa92mF8B4bRW4wX6pq3/preview" width="640" height="480" allow="autoplay"></iframe>', description: "Esto es una descripción ")
Final.create(title:"CURSO DE FINAL", text: '<iframe width="560" height="315" src="https://www.youtube.com/embed/6kxcENOfVvw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>', description: "Esto es una descripción de yt ")

