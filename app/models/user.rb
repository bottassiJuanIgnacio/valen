class User < ApplicationRecord
  authenticates_with_sorcery!

  validates_confirmation_of :password
  validates :first_name, :last_name, presence: true, format: { with: /\A[a-zA-Z\ñÑ\u00C0-\u017F\']+[\s?[a-zA-Z\ñÑ\u00C0-\u017F\']*]*\z/ }
  validates :email, presence: true, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }, if: -> { new_record? || changes[:email] }

  validates :password, length: { minimum: 8 }, if: -> { new_record? || changes[:crypted_password] }
  validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] }

  enum role: %i[administrador estudiante]

  validates :role, presence: true
  
  validates :cursada, inclusion: [true, false]
  validates :final, inclusion: [true, false]

end
