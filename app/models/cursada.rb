class Cursada < ApplicationRecord
  has_many :cursada_contents, dependent: :destroy

  validates :title, presence: true
  validates :description, presence: true, length: { minimum: 10 }
end
