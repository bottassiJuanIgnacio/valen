class Final < ApplicationRecord
  has_many :final_contents, dependent: :destroy

  validates :title, presence: true
  validates :description, presence: true, length: { minimum: 10 }
end
