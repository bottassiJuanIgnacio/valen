class HomesController < ApplicationController
  skip_before_action :require_login

  def index; end
  def curso; end
  def final; end
  def about; end

  def downloadCurso
    pdf_path = Rails.root.join('app', 'assets', 'images', 'cronograma_acompañante_de_cursada.pdf')

    if File.exist?(pdf_path)
      send_file pdf_path, filename: 'cronograma_acompañante_de_cursada.pdf', type: 'application/pdf', disposition: 'attachment'
    else
      flash[:error] = 'El archivo PDF no se encontró.'
      redirect_to root_path
    end
  end

  def downloadFinal
    pdf_path = Rails.root.join('app', 'assets', 'images', 'cronograma_final.pdf')

    if File.exist?(pdf_path)
      send_file pdf_path, filename: 'cronograma_final.pdf', type: 'application/pdf', disposition: 'attachment'
    else
      flash[:error] = 'El archivo PDF no se encontró.'
      redirect_to root_path
    end
  end
end