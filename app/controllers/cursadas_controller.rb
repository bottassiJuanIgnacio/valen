class CursadasController < ApplicationController
  include Pagy::Backend
  include Pundit::Authorization

  def index
    authorize Cursada
    @cursadas = Cursada.all
    @pagy, @cursadas = pagy(Cursada.all)
  end

  def indexProducts
    authorize Cursada
    @cursadas = Cursada.all
  end

  def show
    authorize Cursada
    @cursada = Cursada.find(params[:id])

    @pagy, @cursada_content = pagy(CursadaContent.all)
  end

  def showProduct
    authorize Cursada
    @cursada = Cursada.find(params[:id])
  end

  def new
    authorize Cursada
    @cursada = Cursada.new
  end

  def create
    authorize Cursada
    @cursada = Cursada.new(cursada_params)

    if @cursada.save
      redirect_to @cursada
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize Cursada
    @cursada = Cursada.find(params[:id])
  end

  def update
    authorize Cursada
    @cursada = Cursada.find(params[:id])

    if @cursada.update(cursada_params)
      redirect_to @cursada
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize Cursada
    @cursada = Cursada.find(params[:id])
    @cursada.destroy

    redirect_to cursadas_path, status: :see_other
  end

  private

  def cursada_params
    params.require(:cursada).permit(:title, :description, :text)
  end
end
