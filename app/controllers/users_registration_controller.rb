class UsersRegistrationController < ApplicationController
  before_action :user_logged_in
  skip_before_action :require_login


  def new
    @user = User.new
  end

  def create
    @user = User.new(register_params)
    if @user.save
      auto_login(@user, should_remember = true)
      redirect_to root_path, notice: t('.notice')
    else
      redirect_to register_path
    end
  end

  private

  def register_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end
end