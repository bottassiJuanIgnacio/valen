class UsersController < ApplicationController
  before_action :set_user, only: [:update,:destroy]

  def index
      @users = User.where(role:'estudiante').order(created_at: :asc)

      if params[:cursada] == '1' && params[:final] == '0'
        @users = @users.where(cursada: true, final: false)
      end

      if params[:cursada] == '0' && params[:final] == '1'
        @users = @users.where(cursada: false, final: true)
      end

      if params[:cursada] == '0' && params[:final] == '0'
        @users = @users.where(cursada: false, final: false)
      end

      if params[:reset] == '1'
        redirect_to(request.original_url.gsub(/\?.*/, ''))
      end
      
  end

  def update
    @user.update(user_params)
    if @user.save
      flash[:notice] = "se modifico el usario con exito"
    else
      @user.errors
    end
    redirect_to users_path
  end

  def destroy
    if @user.role == 'estudiante'
      @user.destroy
      flash[:notice] = "se elimino el usuario"
    else
      flash[:alert] = "no se pudo eliminar el usuario"
    end
    redirect_to users_path
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:cursada, :final)
  end

end