class ApplicationController < ActionController::Base
  #before_action :check_signed_in, except: [:login, :reset_password]
  before_action :require_login
  include Pundit::Authorization

  rescue_from Pundit::NotAuthorizedError, with: :not_authorized
  # def check_signed_in #metodo para usar despues con validaciones para que si tiene sesion no pueda iniciar, registrarse o resetear pswd
  #   if logged_in?
  #     redirect_to root_path
  #   end
  # end

  private

  def not_authorized
    redirect_back(fallback_location: root_path)
  end
  
  def not_authenticated
    redirect_to login_path, alert: 'Debes iniciar sesión para acceder a esta página'
  end


  def user_logged_in
    redirect_back(fallback_location: root_path) if logged_in?
  end
end
  