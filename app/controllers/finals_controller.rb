class FinalsController < ApplicationController
  include Pagy::Backend
  include Pundit::Authorization

  def index
    authorize Final
    @finals = Final.all
    @pagy, @finals = pagy(Final.all)
  end

  def indexProducts
    authorize Final
    @finals = Final.all
  end

  def show
    authorize Final
    @final = Final.find(params[:id])

    @pagy, @final_content = pagy(FinalContent.all)
  end

  def showProduct
    authorize Final
    @final = Final.find(params[:id])
  end

  def new
    authorize Final
    @final = Final.new
  end

  def create
    authorize Final
    @final = Final.new(final_params)

    if @final.save
      redirect_to @final
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize Final
    @final = Final.find(params[:id])
  end

  def update
    authorize Final
    @final = Final.find(params[:id])

    if @final.update(final_params)
      redirect_to @final
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize Final
    @final = Final.find(params[:id])
    @final.destroy

    redirect_to finals_path, status: :see_other
  end

  private

  def final_params
    params.require(:final).permit(:title, :description, :text)
  end
end
