class FinalContentsController < ApplicationController
  def create
      @final = Final.find(params[:final_id])
      @final_content = @final.final_contents.create(final_content_params)
      redirect_to final_path(@final)
    end
    
    def edit
      @final = Final.find(params[:final_id])
      @final_content = @final.final_contents.find(params[:id])
    end

    def update
      @final = Final.find(params[:final_id])
      @final_content = @final.final_contents.find(params[:id])
  
      if @final_content.update(final_content_params)
        redirect_to final_path(@final)
      else
        render :edit
      end
    end

    def destroy
      @final = Final.find(params[:final_id])
      @final_content = @final.final_contents.find(params[:id])
      @final_content.destroy
      redirect_to final_path(@final)
    end
  
    private
  
    def final_content_params
      params.require(:final_content).permit(:title, :description, :text)
    end
end
