class UsersSessionController < ApplicationController
  #before_action :check_signed_in
  before_action :redirect_if_logged_in, only: [:new]
  skip_before_action :require_login, except: [:destroy]

  def new; end

  def create
    @user = User.find_by(email: params[:email])
    if @user.present?
      if login(params[:email], params[:password], remember_me = true)
        if @user.administrador?
          redirect_back_or_to(users_path, success: "#{t('.welcome')} #{@user.first_name}")
        elsif (@user.role == "estudiante") && (@user.final == true)
          redirect_back_or_to(producto_finals_path, success: "#{t('.welcome')} #{@user.first_name}")
        elsif (@user.role == "estudiante") && (@user.cursada? == true)
          redirect_back_or_to(producto_cursadas_path, success: "#{t('.welcome')} #{@user.first_name}")
        else
          redirect_back_or_to(root_path, success: "#{t('.welcome')} #{@user.first_name}")
        end
      end
    else
      redirect_to login_path, warning: t('.incorrect_info')
    end
  end

  def destroy
    logout
    redirect_to root_path, notice: 'Has cerrado sesión exitosamente.'
  end

  private

  def redirect_if_logged_in
    redirect_to root_path if logged_in?
  end
end
