class CursadaContentsController < ApplicationController
    include Pagy::Backend
    include Pundit::Authorization

    def create
      authorize CursadaContent
      @cursada = Cursada.find(params[:cursada_id])
      @cursada_content = @cursada.cursada_contents.create(cursada_content_params)
      #@pagy, @cursada_content = pagy(CursadaContent.all)
      redirect_to cursada_path(@cursada)
    end
    
    def edit
      authorize CursadaContent
      @cursada = Cursada.find(params[:cursada_id])
      @cursada_content = @cursada.cursada_contents.find(params[:id])
    end

    def update
      authorize CursadaContent
      @cursada = Cursada.find(params[:cursada_id])
      @cursada_content = @cursada.cursada_contents.find(params[:id])
  
      if @cursada_content.update(cursada_content_params)
        redirect_to cursada_path(@cursada)
      else
        render :edit
      end
    end

    def destroy
      authorize CursadaContent
      @cursada = Cursada.find(params[:cursada_id])
      @cursada_content = @cursada.cursada_contents.find(params[:id])
      @cursada_content.destroy
      redirect_to cursada_path(@cursada)
    end
  
    private
  
    def cursada_content_params
      params.require(:cursada_content).permit(:title, :description, :text)
    end
end
