# frozen_string_literal: true

class CursadaPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    user.administrador?
  end

  def indexProducts?
    (user.estudiante? && user.cursada) || user.administrador? 
  end

  def show?
    user.administrador?
  end

  def showProduct?
    (user.estudiante? && user.cursada) || user.administrador? 
  end

  def new?
    create?
  end

  def create?
    user.administrador?
  end

  def update?
    user.administrador?
  end
  
  def edit?
    update?
  end

  def destroy?
    user.administrador?
  end
end
