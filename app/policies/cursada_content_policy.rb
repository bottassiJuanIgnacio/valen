# frozen_string_literal: true

class CursadaContentPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def create?
    user.administrador?
  end

  def update?
    user.administrador?
  end
  
  def edit?
    update?
  end

  def destroy?
    user.administrador?
  end
end
