class ApplicationMailer < ActionMailer::Base
  default from: email_address_with_name('valenfisiologiaby@gmail.com', 'FisiologiaByValen ')
  layout "mailer"
end
