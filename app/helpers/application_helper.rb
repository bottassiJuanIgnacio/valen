module ApplicationHelper
    include Pagy::Frontend
    # def producto_cursadas_path(user)
    #     "/cursadas/#{cursadas.id}/producto_cursadas"
    # end
    def product_cursadas_path()
      "/producto_cursadas"
    end

    def producto_cursadas_content_path(cursadas)
        "/producto_cursadas/#{cursadas.id}"
    end

    def product_finals_path()
      "/producto_finals"
    end

    def producto_finals_content_path(finals)
        "/producto_finals/#{finals.id}"
    end
end
