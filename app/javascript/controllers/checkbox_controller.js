import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="checkbox"
export default class extends Controller {
  static values = {id: Number }
  
  connect() {
    console.log('Connecting');
    var cursada = document.getElementById(`cursada${this.idValue}`);
    var final = document.getElementById(`final${this.idValue}`);
    this.pre_load(cursada,final);
    cursada.addEventListener('change', () => {
      this.enable(cursada,final);
    })
    final.addEventListener('change', () => {
      this.enable(cursada,final);
    })
  }

  pre_load(cursada,final){
    if (final.checked == true) {
      cursada.disabled = true;
    }
    if (cursada.checked == true){
      final.disabled = true;
    } 

  }
  enable(cursada,final){
    if (cursada.checked == true) {
      final.checked = false;
      final.disabled = true;
    } else if (final.checked == true) {
      cursada.checked = false;
      cursada.disabled = true;
    } else {
      cursada.disabled = false;
      final.disabled = false;
    }
  }
}